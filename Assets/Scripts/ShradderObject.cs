﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShradderObject : MonoBehaviour
{
    


    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Shred")
        {

           // Instantiate(Resources.Load("ShardParticles"), transform.position, transform.rotation);
           // GetComponent<Rigidbody>().AddExplosionForce(Random.Range(10f, 30f), transform.position, Random.Range(1f, 100f), 100f, ForceMode.Impulse);
            Destroy(gameObject.GetComponent<Rigidbody>());
            Debug.Log("Attached");
            Invoke("KillMe", 1f);

        }

    }

    public void KillMe()
    {
        Destroy(gameObject);
    }

}
