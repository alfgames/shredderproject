﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShredderTrigger : MonoBehaviour
{
    public Color objectColor;
    public string objectType;
    private GameObject destroyedVersion;
    private int count = 0,insideCount = 0,soundPerShard,totalPieces;
    AudioSource audioSource;   
    SoundManager soundManager;

    void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        destroyedVersion = transform.GetChild(1).gameObject; // Shards
        audioSource = GetComponent<AudioSource>();
        DynamicShardCalculator();
    }


    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Shred Machine tag : " + collision.gameObject.tag);
        //Debug.Log("Shred Machine name: " + collision.gameObject.name);
        //Debug.Log("Shred Machine contacts tag: " + collision.contacts[0].thisCollider.gameObject.tag);
        //Debug.Log("Shred Machine contacts name: " + collision.contacts[0].thisCollider.gameObject.name);
     
       
        if (collision.contacts[0].thisCollider.gameObject.tag == "shard")
        {

            StartCoroutine("PlaySound");
            GameObject myCollider = collision.contacts[0].thisCollider.gameObject;
            myCollider.AddComponent<Rigidbody>();
            myCollider.GetComponent<Rigidbody>().AddExplosionForce(Random.Range(10f, 16f), transform.position, Random.Range(1f, 350f), 100f, ForceMode.Impulse);
            GameObject particleGameObject = Instantiate(Resources.Load("ShardParticles"), collision.contacts[0].thisCollider.gameObject.transform.position, collision.contacts[0].thisCollider.gameObject.transform.rotation) as GameObject;
            ParticleSystem.MainModule main = particleGameObject.GetComponent<ParticleSystem>().main;
            main.startColor = objectColor;
            main = particleGameObject.transform.GetChild(0).GetComponent<ParticleSystem>().main;
            main.startColor = objectColor;
        }
       
        // Spawn a shattered object
        // Instantiate(destroyedVersion, transform.position, transform.rotation);
        //Debug.Log("collisioned"+collision.gameObject.name);
      


        // Remove the current object
        //Destroy(gameObject);
       

    }


    IEnumerator PlaySound()
    {
        count += 1;
        
        if (objectType == "Glass")
        {
            if (count == 1)
            {
                insideCount = 7;
                audioSource.clip = soundManager.glassSound[insideCount];
                audioSource.Play();
                
            }

        }
        else if (objectType == "Metal")
        {
            if (count == 1)
            {
                insideCount = 6;
                audioSource.clip = soundManager.metalSound[insideCount];
                audioSource.Play();

            }

        }
        else if (objectType == "Plastic")
        {
            if (count == 1)
            {
                audioSource.clip = soundManager.plasticSound[insideCount];
                audioSource.Play();
            }

            if (count % soundPerShard == 0 && count <= totalPieces)
            {
                insideCount += 1;
                audioSource.clip = soundManager.plasticSound[insideCount];
                audioSource.Play();
            }
        }
        else if (objectType == "Wood")
        {
            if (count == 1)
            {
                audioSource.clip = soundManager.woodsSound[insideCount];
                audioSource.Play();             
            }

            if (count % soundPerShard == 0 && count <= totalPieces)
            {  
                insideCount += 1;
                audioSource.clip = soundManager.woodsSound[insideCount];
                audioSource.Play();               
            }
        }

        yield return new WaitForSeconds(audioSource.clip.length);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Shred")
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
            destroyedVersion.gameObject.SetActive(true);
        }
    }

    public void DynamicShardCalculator()
    {
        totalPieces = gameObject.transform.GetChild(1).gameObject.transform.childCount;
        int woodSoundCount = soundManager.woodsSound.Length -1;
        int glassSoundCount = soundManager.glassSound.Length; // one time execute not all length
        int plasticSoundCount = soundManager.plasticSound.Length -1;
        
        if (objectType == "Plastic")
        {
            soundPerShard = (totalPieces / plasticSoundCount);
        }
        else if (objectType == "Wood")
        {
            soundPerShard = (totalPieces / woodSoundCount);
        }
    }


}
